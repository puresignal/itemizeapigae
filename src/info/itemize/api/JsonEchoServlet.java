package info.itemize.api;

import com.google.gson.Gson;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Map;

/**
 * Created by dave on 1/28/15.
 */
//@javax.servlet.annotation.WebServlet(name = "JsonEchoServlet")
public class JsonEchoServlet extends HttpServlet {
    java.util.logging.Logger log = java.util.logging.Logger.getLogger(this.getClass().getCanonicalName());

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        // pre-flight request processing
        resp.setHeader("Access-Control-Allow-Origin", "*");
        resp.setHeader("Access-Control-Allow-Headers", "Content-Type");
        resp.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS, DELETE");
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse rsp) throws ServletException, IOException {
        log.info("doPost");
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        rsp.addHeader("Access-Control-Allow-Headers", "Content-Type");
        rsp.addHeader("Content-Type", "application/json");
        // if File post, return JSON.  if JSON post, return File:
        try {
            ServletFileUpload sfu = new ServletFileUpload(); // set to 10MB for prod deploy...
            sfu.setFileSizeMax(5 * 1024 * 1024); // 5MB max upload for now...
            FileItemIterator fi = sfu.getItemIterator(req);
            if (fi.hasNext()) {
                FileItemStream fis = fi.next();
                InputStream is = fis.openStream();
                String json = IOUtils.toString(is);
                rsp.getWriter().write(json);
                return;
            }
        }
        catch (Exception xcp) {
            log.warning("JSON file import failed: " + xcp.getMessage());
        }
        String ad = req.getParameter("AppData");
        if (ad != null) {
            // AppData comes from hidden form field in hidden form:
            rsp.addHeader("Content-Disposition", "attachment; filename=projects.pw");
            rsp.getWriter().write(req.getParameter("AppData"));
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
