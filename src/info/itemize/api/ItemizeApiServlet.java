package info.itemize.api;

//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import java.io.*;
import java.lang.reflect.Type;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;

import net.sf.mpxj.ProjectFile;
import net.sf.mpxj.ProjectHeader;
import net.sf.mpxj.Task;
import net.sf.mpxj.mpp.MPPReader;
import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import com.google.gson.*;

public class ItemizeApiServlet extends javax.servlet.http.HttpServlet {
    java.util.logging.Logger log = java.util.logging.Logger.getLogger(this.getClass().getCanonicalName());

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        // pre-flight request processing
        resp.setHeader("Access-Control-Allow-Origin", "*");
        resp.setHeader("Access-Control-Allow-Headers", "Content-Type");
        resp.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS, DELETE");
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        if (req.getParameter("testing") == null) {
            resp.setContentType("text/plain");
            resp.getWriter().println("Hello, this is a testing servlet. \n\n");
            Properties p = System.getProperties();
            p.list(resp.getWriter());

        } else {
            UserService userService = UserServiceFactory.getUserService();
            User currentUser = userService.getCurrentUser();

            if (currentUser != null) {
                resp.setContentType("text/plain");
                resp.getWriter().println("Hello, " + currentUser.getNickname());
            } else {
                resp.sendRedirect(userService.createLoginURL(req.getRequestURI()));
            }
        }
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse rsp) throws ServletException, IOException {
        //ServletRequestContext src = new ServletRequestContext(request);
        int clen = req.getContentLength(); // TODO: enforce 10MB max upload
        Enumeration<String> rqh = req.getHeaderNames();
        rsp.addHeader("Access-Control-Allow-Origin", "*");
        rsp.addHeader("Access-Control-Allow-Headers", "Content-Type");
        //rsp.addHeader("Content-Type", "text/html");
        rsp.addHeader("Content-Type", "application/json");
        //rsp.setHeader("Access-Control-Allow-Headers", "Content-Type");
        //rsp.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS, DELETE");
        //rsp.setHeader("Access-Control-Allow-Methods", "*");
        //rsp.setStatus(201);
        try {
            // can't use DFIF on GAE since no disk, but ItemIterator works without it...
            //ServletFileUpload sfu = new ServletFileUpload(new DiskFileItemFactory()); // set to 10MB for prod deploy...
            ServletFileUpload sfu = new ServletFileUpload(); // set to 10MB for prod deploy...
            FileItemIterator fi = sfu.getItemIterator(req);
            if (fi.hasNext()) {
                FileItemStream fis = fi.next();
                FileItemHeaders fih = fis.getHeaders();
                String mtype = fis.getContentType();
                String fname = fis.getName(); // should end in .mpp
                //boolean iff = fis.isFormField();
                InputStream is = fis.openStream();
                MPPReader mpr = new MPPReader();
                mpr.setReadPresentationData(false); // MANDATORY since no awt on GAE!!!
//                ArrayList<Hashtable<String, Object>> tasks = new ArrayList<Hashtable<String, Object>>();
                ArrayList<HashMap<String, Object>> tasks = new ArrayList<HashMap<String, Object>>();
                long start = System.currentTimeMillis();
                ProjectFile pf = mpr.read(is);
                log.info("ProjectFile read ET: " + (System.currentTimeMillis() - start));
                List<Task> allTasks = pf.getAllTasks();
                List<Task> rootTasks = pf.getChildTasks();
//                Hashtable<String,Object> project = new Hashtable<String, Object>();
                HashMap<String,Object> project = new HashMap<String, Object>();
                ProjectHeader h = pf.getProjectHeader();
                int mppVersion = pf.getMppFileType(); // < 9:00, 10:02, 11:03, 12:07, 13:10, 14:13
                // version 9(-11?) have null root (project) task name.
                if (mppVersion < 12) { // set root task to project name
                    allTasks.get(0).setName(h.getProjectTitle());
                }
                for (Task t : allTasks) {
                    // NOTE, array with separate keys array (like .NET DataTable) will be more efficient than Hashtable...
                    // TODO: make it so.
                    // Hashtable doesn't allow null vals!:
//                    Hashtable<String, Object> td = new Hashtable<String, Object>();
                    HashMap<String, Object> td = new HashMap<String, Object>();
                    td.put("title", t.getName());
                    // this is needed for certain MPP versions (<12 ?):
                    if (t.getGUID() == null) {
                        t.setGUID(UUID.randomUUID());
                    }
                    if (t.getParentTask() != null) {
                        //td.put("parentId", t.getParentTask().getUniqueID());
                        // parent GUID should already be set by this point if it was null:
                        td.put("parentId", t.getParentTask().getGUID());
                    }
                    td.put("id", t.getGUID()); // id is typically int.
                    //td.put("id", t.getUniqueID()); // use this if int.
                    td.put("displayid", t.getID()); // not required for kendo.data.GanttTask
                    td.put("level", t.getOutlineLevel()); // not required
                    td.put("expanded", t.getExpanded());
                    td.put("start", t.getStart());
                    td.put("finish", t.getFinish());
//                    Date actfin = t.getActualFinish();
//                    td.put("actualFinish", actfin);
                    td.put("actualFinish", t.getActualFinish());
                    td.put("summary", t.getSummary());
                    td.put("percentComplete", t.getPercentageComplete());
                    td.put("notes", t.getNotes());
                    td.put("hyperlink", t.getHyperlink());
                    td.put("hyperlinkAddr", t.getHyperlinkAddress());
                    td.put("hyperlinkSubAddr", t.getHyperlinkSubAddress());
                    String oln = t.getOutlineNumber();
                    int sibindex = Integer.parseInt(oln.substring(oln.lastIndexOf(".")+1));
                    td.put("orderId", Math.max(0, sibindex-1)); // == sibindex, supposedly required for kendo.data.GanttTask?
                    //td.put("outlineNumber", sibindex-1);
                    tasks.add(td);
                }
                //project.put("header", pf.getProjectHeader()); // not serializable...
                project.put("author", h.getAuthor());
                project.put("created", h.getCreationDate());
                //String appName = h.getApplicationName();
                project.put("appName", pf.getApplicationName());
                project.put("mppFileType", pf.getMppFileType());
                project.put("title", h.getProjectTitle());
                //project.put("contentType", h.getContentType()); // nulls crash Java.
                project.put("tasks", tasks);
                Gson gson = new Gson();
                String json = gson.toJson(project);
                //rsp.getWriter().write(gson.toJson(tasks));
                rsp.getWriter().write(json);
            }
        }
        catch (Exception xcp) {
            log.warning("post MPP failed: " + xcp.getMessage());
        }
    }
}
