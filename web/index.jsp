<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%
//  UserService userService = UserServiceFactory.getUserService();
//  String thisURL = request.getRequestURI();
  %>
<!DOCTYPE html>
<html ng-controller="MainController">
<head>
  <title>Itemize.Info</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <%--<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>--%>
  <script src="lib/require.js"></script>
  <script src="js/requireConfig.js"></script>
  <link href="lib/bs/css/bootstrap.min.css" rel="stylesheet">
  <link href="lib/bs/css/bootstrap-theme.min.css" rel="stylesheet">
  <link rel="stylesheet" href="lib/k/css/kendo.common.min.css" />
  <link rel="stylesheet" href="lib/k/css/kendo.default.min.css" />
  <link rel="stylesheet" href="lib/k/css/kendo.dataviz.min.css" />
  <link rel="stylesheet" href="lib/k/css/kendo.dataviz.default.min.css" />
  <link rel="stylesheet" href="css/app.css" />
</head>
<body role="document">
<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#/">ScopeWright</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="#/projects">Projects</a></li>
        <li style="cursor: pointer;"><a ng-click="app.save($root, $root.http)">Save</a></li>
        <%--<li style="cursor: pointer;"><a ng-click="app.clear()">Clear</a></li>--%>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li class="dropdown-header">Nav header</li>
            <li><a href="#">Separated link</a></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>
      <div style="float: right;">
        <ul class="nav navbar-nav">
          <li><span style="background-color: #fe8; color: black;" id="Message">loading...</span></li>
          <%--<li><a href="<%= userService.createLogoutURL(thisURL) %>"><%= request.getUserPrincipal().getName() %></a></li>--%>
        </ul>
      </div>
    </div><!--/.nav-collapse -->
  </div>
</nav>
  <div class="container" role="main" ui-view></div>
<form id="DataForm" action="echo" method="post">
  <input name="AppData" type="hidden" id="AppData" value="hidden value" />
</form>
</body>
</html>
