require.config({
    paths: {
        app: 'js/app',
        model: 'js/model',
        //util: 'js/util',
        util: 'js/util-built',
        angular: 'lib/k/js/angular.min',
        bootbox: 'lib/bootbox.min', // could just use modal Kendo window...
        bootstrap: 'lib/bs/js/bootstrap.min',
        jquery: 'lib/k/js/jquery.min',
        //kendo: 'lib/k/js/kendo.all.min',
        // kendo.custom (angular,upload,gantt) is 1/3 kendo.all (650k / 1.9m):
        kendo: 'lib/k/js/kendo.custom.min',
        uiRouter: 'lib/angular-ui-router.min'
        // localforage, uuid in util-built (r.js optimized)
        // https://github.com/mozilla/localForage/issues/58
        //localforage: 'lib/localforage.min',
        //uuid: 'lib/uuid'
    },
    shim: {
        'angular': ['jquery'],
        'bootbox': ['jquery', 'bootstrap'],
        'bootstrap': ['jquery'],
        'kendo': ['angular', 'jquery']
    },
    waitSeconds: 10
});

require([
    'app',
    'angular',
    'bootbox',
    'bootstrap',
    'jquery',
    'kendo',
    'model',
    'uiRouter',
    'util'
], function() {
    angular.bootstrap(document, ['app']); // manually bootstrap app after dep load...
});
