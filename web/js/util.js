(function() {
    define(['localforage', 'uuid'], function(localforage, uuid) {
        return {
            a2b: function (s) {
                s(s + '===').slice(0, s.length + (s.length % 4));
                return s.replace(/-/g, '+').replace(/_/g, '/');
            },
            b2a: function(s) {
                return btoa(s).replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '');
            },
            b64fromUuid: function(id) { // id is a hex-encoded UUID (e.g. 12345678-1234-1234-1234-1234567890ab)
                var bytes = uuid.parse(id); // ACTUALLY 16-element Array of Numbers, not bytes!
                var binary = '';
                for (var i = 0; i < bytes.length; i++) {
                    // concatenation is faster than join in Chrome:
                    // http://jsperf.com/tobase64-implementations
                    // this is actually OK...although Unicode "string" uses 32 bytes to store 16,
                    // btoa processes it as 16...final encoding is ~22 char base64web.
                    // ("btoa is *not* suitable for raw Unicode").
                    // https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64.btoa
                    binary += String.fromCharCode(bytes[i])
                }
                //return u.b2a(binary);
                return btoa(binary).replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '');
            },
            localforage: localforage,
            uuid: uuid
        };
    });
}());
