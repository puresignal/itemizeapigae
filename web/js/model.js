(function() {
    // NOTE: prefix transient properties with $ (e.g. $p(arent)) to prevent Angular serialization..
    // TODO: consider using Angular to strip these prior to local storage.
    define(['kendo', 'util'], function(kendo, util) {
        var m = {
            // App, optionally from persisted config:
            // TODO: include apiRoot in config
            //var apiRoot = 'http://localhost:33113/'; // TODO: auto-sense apiRoot from host
            //var apiRoot = 'http://api.bitwise.us/'; // TODO: auto-sense apiRoot from host

            App: function (config) {
                this.projects = config ? config.projects : [];
                this.projectHash = config ? config.projectHash : {};
                this.currentWbs = {};
                this.selectedProject = {};
            },

            // currently (2015-01-17) only used for MPP import:
            Project: function(config) {
                console.log('Project config:', config);
                this.ctor = 'Project'; // for reconstructing typed Object after localforage persistence
                // TODO: unpack flat tasklist, attach to this.root.
                this.tasks = config.tasks; // serialized, *FLAT* list of dictionaries
                for (var i=0; i < this.tasks.length; i++) {
                    var t = this.tasks[i];
                    t.start = t.start ? new Date(t.start) : null; // Gson date string must be coerced to JS Date.
                    t.finish = t.finish ? new Date(t.finish) : null; // *NOTE*, "finish" == *planned* finish
                    t.finishActual = t.finishActual ? new Date(t.finishActual) : null;
                }
                //this.id = util.b64fromUuid(this.tasks[0].id);
                this.id = util.b64fromUuid(this.tasks[0].id);
                this.name = config.title || this.tasks[0].title;
            },

            KendoGanttOptions: function($rootScope) {
                //dataSource: $scope.project.ganttData,
                //dependencies: dependenciesDataSource,
                return {
                dependencies: new kendo.data.GanttDependencyDataSource({
                    data: [],
                    schema: {
                        model: {
                            id: "id",
                            fields: {
                                id: { from: "id", type: "string" },
                                predecessorId: { from: "predecessorId", type: "string" },
                                successorId: { from: "successorId", type: "string" },
                                type: { from: "Type", type: "number" }
                            }
                        }
                    }
                }),
                views: [
                    "day",
                    { type: "week", selected: true },
                    "month"
                ],
                columns: [
                    //{ field: "id", title: "ID", width: 60 },
                    { field: "title", title: "Task Name", editable: true }
                    //{ field: "start", title: "Start", format: "{0:M/dd/yy}", width: 80 },
                    //{ field: "end", title: "Finish", format: "{0:M/dd/yy}", width: 80 }
                ],
                add: function(e) {
                    e.task.id = util.uuid.v4();
                },
                dataBound: function(e) {
                    $rootScope.currentWbs = e.sender;
                },
                height: 600,
                remove: function(e) {
                    // TODO: autoSave option to immediately write changes
                    // and/or undo log
                    //console.log('removing task: ', e.task);
                },
                showWorkHours: false,
                showWorkDays: false
            }
            }
        };

        m.App.prototype.clear = function() {
            util.localforage.removeItem('appState', function(err) {
                console.log('removed appState');
            });
        };

        m.App.prototype.save = function($rs) {
            var self = this;
            //console.log('http:', $rs.http);
            var p = self.selectedProject;
            // currentWbs in $rs to eliminate save/rebuild issues from hooking to app (this)
            var wbs = $rs.currentWbs;
            // prepare tasks (GanttDataSource.data()) for saving:
            if (wbs) {
                p.tasks = eval(angular.toJson(wbs.dataSource.data()));
            }
            var selfdata = angular.copy(self);
            console.log('selfdata:', selfdata);
            var f = document.getElementById('DataForm');
            var fd = document.getElementById('AppData');
            fd.value = angular.toJson(this);
            f.submit();
            //$rs.http({
            //    method: 'POST',
            //    url: '/echo',
            //    data: selfdata
            //})
            //.success(function (d) {
            //    console.log('Saved.  Returned:', d);
            //})
            //.error(function (data, status, errors, config) {
            //    console.log('ERROR:', data);
            //});
        }

        m.App.prototype.saveOrig = function($rs) { // app is set in $rootScope...
            var p = this.selectedProject;
            // currentWbs in $rs to eliminate save/rebuild issues from hooking to app (this)
            var wbs = $rs.currentWbs;
            // prepare tasks (GanttDataSource.data()) for saving:
            if (wbs) {
                p.tasks = eval(angular.toJson(wbs.dataSource.data()));
            }
            var start = new Date();
            $('#Message').html('saving...');
            util.localforage.setItem('appState', this, function(err, val) {
                var end = new Date();
                console.log('saved appState in ' + (end-start) + ' ms:', err, val);
                $rs.$timeout(function() {
                    $('#Message').html('');
                }, 400); // observable delay
            });
        };

        m.Project.prototype.GanttData = function() {
            return new kendo.data.GanttDataSource({
                change: function(e) {
                    //console.log('GDS changed:', e);
                },
                data: this.tasks,
                schema: {
                    model: {
                        id: "id",
                        fields: {
                            id: { from: "id", type: "string" },
                            orderId: { from: "orderId", type: "number", validation: { required: true } },
                            parentId: { from: "parentId", type: "string", validation: { required: true } },
                            start: { from: "start", type: "date" },
                            // apparently end is required?
                            //finish: { from: "finish", type: "date" },
                            end: { from: "finish", type: "date" },
                            title: { from: "title", defaultValue: "", type: "string" },
                            percentComplete: { from: "percentComplete", type: "number" },
                            summary: { from: "summary" },
                            expanded: { from: "expanded" }
                        }
                    }
                }
            });
        }
        return m;
    });
}());
