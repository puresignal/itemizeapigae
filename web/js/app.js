(function () {
    // explicitly including bootstrap for r.js optimizer, which is unaware of bootbox shim
    define(['model', 'util', 'bootbox', 'bootstrap', 'angular', 'uiRouter'], function (m, util, bootbox) {
        angular.module('app', ['kendo.directives', 'ui.router'])
            .controller('MainController', function ($rootScope, $scope, $http, $location, $timeout) {
                $rootScope.http = $http; // for use in model...
                $rootScope.$timeout = $timeout; // for use in model...
                $('#Message').html('loading app data...');
                // check localforage for saved appstate, else generate blank / fake:
                util.localforage.getItem('appState', function (err, val) {
                    $timeout(function () {
                        console.log('appState:', val);
                        $rootScope.app = new m.App(val || undefined); // val, if defined acts as config for App ctor.
                        $('#Message').html('');
                        $rootScope.$broadcast('appLoaded', []);
                        // TODO: call ctors recursively for reconstituted Projects, Tasks.  Else they remain generic Objects.
                        // TODO: consider saving projects independently outside of appState.  Then use appState.loadedProjects prop to preload
                        // TODO: dropdown to navigate between multiple open projects?
                    });
                });
                if (!$location.path()) { // redirect bare root
                    $location.path('/');
                }
            })
            .controller('HomeController', function ($rootScope, $scope) {
                $scope.greeting = 'Hello, World!';
            })
            .controller('GanttController', function ($rootScope, $scope, $location, $timeout, $stateParams) {
                //$scope.projectId = $stateParams.id;
                // *NOTE*, projectHash is populated by project list view.
                // TODO: load app.projectList and app.projectHash in MainController.
                // may want to lazy-load projectHash for performance reasons eventually.
                // only preLoad app.lastOpenProject, app.currentProject or something like that.

                // defer dataSource until project loaded...
                // TODO: consider setting fake dataSource in options to set schema string ids.
                // otherwise they default to numbers when using setDataSource, breaking everything.
                $scope.ganttOptions = m.KendoGanttOptions($rootScope);

                //$scope.project = $rootScope.app.projectHash[$stateParams.id];
                // this method requires app pre-init:
                if ($rootScope.app) {
                    var pdata = $rootScope.app.projectHash[$stateParams.id];
                    $rootScope.app.selectedProject = pdata;
                    $scope.ganttOptions.dataSource = new m.Project(pdata).GanttData();
                }
                // this way handles refreshing task/project without pre-init / nav (e.g. bookmark / browser refresh):
                $scope.$on('appLoaded', function ($evt, data) { // handle async load from MainController...
                    var pdata = $rootScope.app.projectHash[$stateParams.id];
                    $rootScope.app.selectedProject = pdata;
                    $scope.wbs.setDataSource(new m.Project(pdata).GanttData());
                    $scope.wbs._dropDowns(); // need to re-init dropDowns to create with gds schema!!! (otherwise defaults to int id!!!)
                    $rootScope.currentWbs = $scope.wbs;
                });

                $timeout(function () {
                    $rootScope.currentWbs = $scope.wbs;
                });

            })
            .controller('ProjectListController', function ($rootScope, $scope, $location, $timeout) {
                $scope.options = {
                    Upload: {
                        async: {saveUrl: 'mpp', autoUpload: true},
                        //async: {saveUrl: 'echo', autoUpload: true},
                        complete: function () {
                            // still no way to reset, so removing "Done [x]" element manually:
                            // http://www.telerik.com/forums/reset-file-upload-after-successful-upload-993b15088551
                            //$('.k-upload').addClass('k-upload-empty');
                            $('.k-dropzone').children('strong').remove();
                        },
                        multiple: false,
                        success: function (e) {
                            $scope.onSuccess(e)
                        },
                        showFileList: false
                    }
                };

                $scope.del = function (p) {
                    //if (confirm('Are you sure you want to delete the project?')) {
                    //    $rootScope.app.projects.splice($rootScope.app.projects.indexOf(p), 1);
                    //}
                    bootbox.confirm('Are you sure you want to delete the project?', function (yes) {
                            if (yes) {
                                $timeout(function () {
                                    $rootScope.app.projects.splice($rootScope.app.projects.indexOf(p), 1);
                                });
                            }
                        }
                    )
                    //alert('deleting item #' + $rootScope.app.projects.indexOf(p));
                };

                $scope.onSuccess = function (e) {
                    console.log('response:', e.response);
                    //console.log('JSON file echo back:', e); // not handling arbitrary JSON echo here, only MPP import.
                    $timeout(function () {
                        var p = new m.Project(e.response);
                        $rootScope.app.projectHash[p.id] = p;
                        $rootScope.app.projects.push(p);

                        // TODO: tilde-separated /#task/url-safe-task-name~b64uid
                        // tilde is 'unreserved' URL character; hyphen and underscore are used for b64url:
                        // https://tools.ietf.org/html/rfc3986
                    });
                }
            })
            .config(function ($stateProvider) {
                $stateProvider
                    .state('home', {
                        url: '/',
                        controller: 'HomeController',
                        templateUrl: 't/home.html'
                    })
                    // TODO: prefix or suffix :id with URL-friendly item name.
                    .state('task', {
                        url: '/t/:id', // TODO: /task/friendly-task-name~:id where id is base64url-encoded GUID
                        controller: 'GanttController',
                        templateUrl: 't/task.html'
                    })
                    .state('projects', {
                        url: '/projects', // TODO: consider singular 'project' here
                        controller: 'ProjectListController',
                        templateUrl: 't/projects.html'
                    })
            });
    });
}());
